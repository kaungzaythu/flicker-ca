//
//  FlickrCAPhotoCell.swift
//  FlickrCA
//
//  Created by Student on 8/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit

class FlickrCAPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
   
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    struct getIndexPath {
        static var indexPath: NSIndexPath!
        static var searches = [FlickrSearchResults]()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selected = false
    }
    
    override var selected : Bool {
        didSet {
            self.backgroundColor = selected ? themeColor : UIColor.whiteColor()
        }
    }
    
    func getPhoto(indexPath: NSIndexPath) -> FlickrPhoto{
        return FlickrCAPhotoCell.getIndexPath.searches[indexPath.section].searchResults[indexPath.row]
    }
    
}

