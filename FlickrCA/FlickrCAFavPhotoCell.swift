//
//  FlickrCAFavPhotoCell.swift
//  FlickrCA
//
//  Created by Student on 9/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit

class FlickrCAFavPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var lbltite: UILabel!
    @IBOutlet weak var lblcomment: UILabel!
    @IBOutlet weak var lblid: UILabel!
    @IBOutlet weak var lblurl: UILabel!
    @IBOutlet weak var img: UIImageView!
}
