//
//  FlickrCAHeaderView.swift
//  FlickrCA
//
//  Created by Student on 7/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//
//// ****************** Not Use ******************./////
import Foundation
import UIKit

class FlickrCAHeaderView: UICollectionReusableView {
    @IBOutlet weak var label: UILabel!
    private var selectedPhotos = [FlickrPhoto]()
    private let shareTextLabel = UILabel()
    
    func updateShardPhotoCount() {
        shareTextLabel.textColor = themeColor
        shareTextLabel.text = "\(selectedPhotos.count) photos selected"
        shareTextLabel.sizeToFit()
    }
    
}
