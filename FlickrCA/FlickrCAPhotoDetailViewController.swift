//
//  FlickrCAPhotoDetailViewController.swift
//  FlickrCA
//
//  Created by Student on 9/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit

class FlickrCAPhotoDetailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imageDetail: UIImageView!
    
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var txtComment: UITextField!
    
    @IBOutlet weak var btnFavourite: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var formtitle: UILabel!
    
    @IBOutlet weak var bbi: UIBarButtonItem!
    
    @IBAction func goBack(segue:UIStoryboardSegue){
     //self.performSegueWithIdentifier("goBack", sender: nil)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func goBack1(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    var id: String!
    var url: String!
    var imgtitle: String!
    var comment: String!
    var img: UIImage?
    var mySearchkey: String!
    var btnFavcheck: Bool!
    var btnUpdatecheck: Bool!
    var form_title: String!
    var check404:Bool = false
    
    var flickrDB : COpaquePointer = nil;
    var insertStatement : COpaquePointer = nil;
    var updateStatement : COpaquePointer = nil;
    var deleteStatement : COpaquePointer = nil;
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        txtComment.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openDB()
        
        self.lbltitle.text = imgtitle
        self.imageDetail.image = img
        txtComment.delegate = self
        
        if (btnUpdatecheck == true){
            self.txtComment.text = comment
            btnFavourite.hidden = true
            
            formtitle.text = form_title
            println("id in detail is \(id)")
        }else if (btnFavcheck == true){
            if(check404 == true){
            btnFavourite.hidden = true
                txtComment.hidden = true
            }
            else
            {
                check404 = false
            }
            formtitle.text = form_title
            btnUpdate.hidden = true
            btnDelete.hidden = true
        }
    }
    func openDB(){
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)[0] as String
        var docsDir = paths.stringByAppendingPathComponent("flickr.sqlite")
        println(docsDir)
        if(sqlite3_open(docsDir, &flickrDB) == SQLITE_OK){
            var sql = "CREATE TABLE IF NOT EXISTS FLICKR_FAVOURITE (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT, COMMENT TEXT, IMGURL TEXT)"
            
            var statement:COpaquePointer = nil
            if(sqlite3_exec(flickrDB, sql, nil, nil, nil) != SQLITE_OK){
                println("Failed to create table")
                println(sqlite3_errmsg(flickrDB));
            }
        }else{
            println("Failed to open table")
            println(sqlite3_errmsg(flickrDB));
        }
        prepareStatement();
    }
    
    func prepareStatement(){
        var sqlString: String
        sqlString = "INSERT INTO FLICKR_FAVOURITE (TITLE, COMMENT, IMGURL) VALUES (?,?,?)"
        var cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &insertStatement, nil)
        
        sqlString = "UPDATE FLICKR_FAVOURITE SET COMMENT = ? WHERE ID = ?"
        cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &updateStatement, nil)
        
        sqlString = "DELETE FROM FLICKR_FAVOURITE WHERE ID = ?";
        cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &deleteStatement, nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "searchKey"){
           (segue.destinationViewController as FlickrCAResultViewController).searchkey = self.mySearchkey
            
        }
    }
    
    @IBAction func share(){
        let x = imageDetail.image! as UIImage
        let y: UIActivityViewController = UIActivityViewController(activityItems: [x], applicationActivities: nil)
        self.presentViewController(y, animated: true, completion: nil)
    
        
    }
    
    @IBAction func save(){
        txtComment.resignFirstResponder()
        var commentstr = (txtComment.text as NSString).UTF8String
        var titlestr = (self.imgtitle as NSString).UTF8String
        sqlite3_bind_text(insertStatement, 1, titlestr, -1, nil);
        sqlite3_bind_text(insertStatement, 2, commentstr, -1, nil);
        sqlite3_bind_text(insertStatement, 3, self.url, -1, nil);
        if(sqlite3_step(insertStatement) == SQLITE_DONE)
        {
            println("success")
        }else{
            println("fail")
        }
        sqlite3_reset(insertStatement);
        sqlite3_clear_bindings(insertStatement);
    dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func update(sender: AnyObject)
    {
        //println("upid is \(id)")
        //println("upcomment is \(txtComment.text)")
        var upid = (self.id as NSString).UTF8String
        var upcomment = (txtComment.text as NSString).UTF8String
        sqlite3_bind_text(updateStatement, 1, upcomment, -1, nil)
        sqlite3_bind_text(updateStatement, 2, upid, -1, nil)
        
        if(sqlite3_step(updateStatement) == SQLITE_DONE){
            println("fav update success ")
            //println(updateStatement)
        }else{
            println("fav update fail ")
        }
        sqlite3_reset(updateStatement);
        sqlite3_clear_bindings(updateStatement);
        
    }
    
    @IBAction func deleteContact(sender: AnyObject){
        var delID = (self.id as NSString).UTF8String
        sqlite3_bind_text(deleteStatement, 1, delID, -1, nil);
        if(sqlite3_step(deleteStatement) == SQLITE_DONE)
        {
            println("Delete Success")
           
        }else{
            println("Delete Fail")
        }
        sqlite3_reset(deleteStatement);
        sqlite3_clear_bindings(deleteStatement);
      
    }
    
}
