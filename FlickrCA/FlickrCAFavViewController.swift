//
//  FlickrCAFavViewController.swift
//  FlickrCA
//
//  Created by Student on 9/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit

let reuseIdentifier = "FavId"

class FlickrCAFavViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    private var searches = [FlickrSearchResults]()
    var flickrDB : COpaquePointer = nil;
    var selectStatement : COpaquePointer = nil;
    var photoArray : NSMutableArray! = [];
    var commentArray : NSMutableArray! = [];
    var titleArray : NSMutableArray! = [];
    var idArray : NSMutableArray! = [];
    var img:UIImage?
    var lbltitlex:UILabel?
  
    
    var id_url: String!
    var photo_url: String!
    var lbltitle: String!
    var txtComment: String!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openDB()
        selectAllPhoto(self);
    }

    func photoForIndexPath(indexPath: NSIndexPath) -> FlickrPhoto {
        return searches[indexPath.section].searchResults[indexPath.row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return photoArray.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as FlickrCAFavPhotoCell
    
        id_url = idArray[indexPath.row] as String
        
        photo_url = photoArray[indexPath.row] as String
        lbltitle = titleArray[indexPath.row] as String
        
        txtComment = commentArray[indexPath.row] as String
        
        
        
     
        
        cell.lbltite.text = lbltitle
        cell.lblcomment.text = txtComment
        
        cell.lblurl.text = photo_url
        cell.lblid.text = id_url
        
        cell.lblurl.hidden = true
        cell.lblid.hidden = true
       
        img = UIImage(data: NSData(contentsOfURL: NSURL(string: photo_url)!)!)
      
        cell.img.image = img
        return cell
    }
    

    
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    func openDB(){
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)[0] as String
        var docsDir = paths.stringByAppendingPathComponent("flickr.sqlite")
        
        if(sqlite3_open(docsDir, &flickrDB) == SQLITE_OK){
            var sql = "CREATE TABLE IF NOT EXISTS FLICKR_HISTORY (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT)"
            println("database open success")
            var statement:COpaquePointer = nil
            if(sqlite3_exec(flickrDB, sql, nil, nil, nil) != SQLITE_OK){
                println("Failed to create table")
                println(sqlite3_errmsg(flickrDB));
            }
        }else{
            println("Failed to open table")
            println(sqlite3_errmsg(flickrDB));
        }
        prepareStatement();
    }
    func prepareStatement(){
        var sqlString: String
        
        sqlString = "SELECT * FROM FLICKR_FAVOURITE"
        var cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &selectStatement, nil)
    }
    
    func selectAllPhoto(sender: AnyObject){
        photoArray.removeAllObjects()
        while(sqlite3_step(selectStatement) == SQLITE_ROW){
            var myDictionary = NSMutableDictionary()
            
            let id_buf = sqlite3_column_text(selectStatement,0)
            let title_buf = sqlite3_column_text(selectStatement,1)
            let comment_buf = sqlite3_column_text(selectStatement,2)
            let url_buf = sqlite3_column_text(selectStatement,3)
            
            idArray.addObject(String.fromCString(UnsafePointer<CChar>(id_buf))!)
            titleArray.addObject(String.fromCString(UnsafePointer<CChar>(title_buf))!)
            photoArray.addObject(String.fromCString(UnsafePointer<CChar>(url_buf))!)
        commentArray.addObject(String.fromCString(UnsafePointer<CChar>(comment_buf))!)
            
        }
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "FavDetail"){
            let cell = sender as FlickrCAFavPhotoCell
            let row = collectionView?.indexPathForCell(cell)
            let vc = segue.destinationViewController as FlickrCAPhotoDetailViewController
            //let photo = photoForIndexPath(indexPath!)
            let indexPath: NSIndexPath = row!
            
            vc.id = idArray[indexPath.row] as String
            
            
            println("id_url is \(id_url)")
            vc.url = photo_url
            var mytitle = titleArray[indexPath.row] as String
            
            
            vc.imgtitle = mytitle
            var myurl = photoArray[indexPath.row] as String
            
            img = UIImage(data: NSData(contentsOfURL: NSURL(string: myurl)!)!)
            
            vc.img = img
            vc.comment = commentArray[indexPath.row] as String
            vc.form_title = "Update Favourite"
            vc.btnUpdatecheck = true
            vc.btnFavcheck = false
           // vc.bbi
        }
    }
}
