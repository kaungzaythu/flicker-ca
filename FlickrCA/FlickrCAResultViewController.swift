//
//  FlickrCAResultViewController.swift
//  FlickrCA
//
//  Created by Student on 7/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit

class FlickrCAResultViewController:UIViewController,
    UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var ai: UIActivityIndicatorView!
    
    @IBOutlet weak var SearchTitle: UILabel!
    @IBOutlet weak var SearchResultFor: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var searchkey:String!
    let myflickr = FlickrCASearchModel()
    var flickrCASearchHistory = FlickrCASearchHistory()
    private var searches = [FlickrSearchResults]()
    private let reuseIdentifier = "PhotoCell"
    private let flickr = FlickrCASearchModel()
    var searches2 = [FlickrSearchResults]()
    var titleArray : NSMutableArray! = [];
    
    var abc = NSArray()
    func photoForIndexPath(indexPath: NSIndexPath) -> FlickrPhoto {
        return searches[indexPath.section].searchResults[indexPath.row]
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.ai.startAnimating()
        self.SearchTitle.text = searchkey;
        //println("key \(self.searchkey)")
        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
       
            flickr.searchFlickrForTerm(searchkey) { results, error in
                if error != nil {
                    println("Error searching : \(error)")
                }
                if results != nil {
                    println("Found \(results!.searchResults.count) matching \(results!.searchTerm)")
                    self.ai.stopAnimating()
                    self.ai.hidden = true
                    
                    //println("results is \(results)");
                   // self.abc = results?.searchResults!
                    
                    if((results!.searchResults.count) == 0){
                        self.SearchTitle.hidden = true;
                        self.SearchResultFor.text = "Sorry.. Not found";
                    }
                    self.searches.insert(results!, atIndex: 0)
                    
                }
                
                self.collectionView?.reloadData()
                //self.addingtoArray(self)
            }
            
        
        
    }
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return searches.count
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searches[section].searchResults.count
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(
            reuseIdentifier, forIndexPath: indexPath) as FlickrCAPhotoCell
        let flickrPhoto = photoForIndexPath(indexPath)
        let buf_title = flickrPhoto.title
       // println(buf_title)
        if indexPath != largePhotoIndexPath {
            cell.imageView.image = flickrPhoto.thumbnail
            cell.lblTitle.text = flickrPhoto.title
            
            return cell
        }
        if flickrPhoto.largeImage != nil {
            cell.imageView.image = flickrPhoto.largeImage
            cell.lblTitle.text = flickrPhoto.title
            return cell
        }
        cell.imageView.image = flickrPhoto.thumbnail
    
        //cell.activityIndicator.startAnimating()
        flickrPhoto.loadLargeImage {
            loadedFlickrPhoto, error in
            //cell.activityIndicator.stopAnimating()
            if error != nil {
                return
            }
            if loadedFlickrPhoto.largeImage == nil {
                //cell.imageView.image = flickrPhoto.thumbnail
                return
            }
            if indexPath == self.largePhotoIndexPath {
                if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? FlickrCAPhotoCell {
                    cell.imageView.image = loadedFlickrPhoto.largeImage
                }
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView!,
        layout collectionViewLayout: UICollectionViewLayout!,
        sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        
            let flickrPhoto = photoForIndexPath(indexPath)
            
            if indexPath == largePhotoIndexPath {
                var size = collectionView.bounds.size
                size.height -= topLayoutGuide.length
                size.height -= (sectionInsets.top + sectionInsets.right)
                size.width -= (sectionInsets.left + sectionInsets.right)
                return flickrPhoto.sizeToFillWidthOfSize(size)
            }
            if var size = flickrPhoto.thumbnail?.size {
                size.width += 10
                size.height += 10
                return size
            }
            
            return CGSize(width: 100, height: 100)
    }
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    func collectionView(collectionView: UICollectionView!,
        layout collectionViewLayout: UICollectionViewLayout!,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
 
   
        
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "PhotoDetail"){
            let cell = sender as FlickrCAPhotoCell
            let indexPath = self.collectionView.indexPathForCell(cell)
            
            let photo = photoForIndexPath(indexPath!)
            let vc = segue.destinationViewController as FlickrCAPhotoDetailViewController
            vc.id = photo.photoID
            vc.imgtitle = photo.title
            vc.url = photo.url
        
            println(NSURL(string: photo.url))
            if (vc.url == ""){
                println("don't")
                var path: String!
                path = NSBundle.mainBundle().pathForResource("404", ofType: "png")
                println("path is \(path)")
                vc.url = path
                vc.img = UIImage(contentsOfFile: path)
                //vc.btnFavourite.hidden = true
                vc.check404 = true
            }
            else
            {
                println("do")
                vc.img = UIImage(data: NSData(contentsOfURL: NSURL(string: photo.url)!)!)
                println("Copy...\(vc.url)")
            }
            
            
            
            vc.mySearchkey = searchkey
            vc.form_title = "Add to your favourite"
            vc.btnUpdatecheck = false
            vc.btnFavcheck = true
            
        }
    }
    @IBAction func goBack1(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    var largePhotoIndexPath : NSIndexPath? {
        didSet {
            var indexPaths = [NSIndexPath]()
            if largePhotoIndexPath != nil {
                indexPaths.append(largePhotoIndexPath!)
            }
            if oldValue != nil {
                indexPaths.append(oldValue!)
            }
            collectionView?.performBatchUpdates({
                self.collectionView?.reloadItemsAtIndexPaths(indexPaths)
                return
                }){
                    completed in
                    if self.largePhotoIndexPath != nil {
                        self.collectionView?.scrollToItemAtIndexPath(
                            self.largePhotoIndexPath!,
                            atScrollPosition: .CenteredVertically,
                            animated: true)
                    }
            }
        }
    }

}