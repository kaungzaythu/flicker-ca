//
//  FlickrCARecentViewController.swift
//  FlickrCA
//
//  Created by Student on 7/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import Foundation
import UIKit
class FlickerCARecentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate{
    @IBOutlet var tableview: UITableView!
    var objects = NSMutableArray()
    var flickrHistory = FlickrCASearchHistory()
    var flickrDB : COpaquePointer = nil;
    var deleteStatement : COpaquePointer = nil;
    var selectStatement : COpaquePointer = nil;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Recent Search"
        println("you enter view did load")
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        openDB();
        loadHistoryTable(self);
   
    }
    func openDB(){
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)[0] as String
        var docsDir = paths.stringByAppendingPathComponent("flickr.sqlite")
        
        if(sqlite3_open(docsDir, &flickrDB) == SQLITE_OK){
            var sql = "CREATE TABLE IF NOT EXISTS FLICKR_HISTORY (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT)"
            println("database open success")
            var statement:COpaquePointer = nil
            if(sqlite3_exec(flickrDB, sql, nil, nil, nil) != SQLITE_OK){
                println("Failed to create table")
                println(sqlite3_errmsg(flickrDB));
            }
        }else{
            println("Failed to open table")
            println(sqlite3_errmsg(flickrDB));
        }
        prepareStatement();
    }
    func loadHistoryTable(sender: AnyObject){
        
        sqlite3_bind_text(selectStatement, 1, nil, -1, nil)
        println("you are in loadHistoryTable")
        while(sqlite3_step(selectStatement) == SQLITE_ROW)
        {
            
            let name = sqlite3_column_text(selectStatement, 1)
            let name_buf = String.fromCString(UnsafePointer<CChar>(name))
            objects.addObject(name_buf!)
            
        }
        sqlite3_reset(selectStatement);
        sqlite3_clear_bindings(selectStatement);
        
    }
    func prepareStatement(){
        var sqlString: String
        
        sqlString = "SELECT * FROM FLICKR_HISTORY"
        var cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &selectStatement, nil)
        println("\(sqlString)")
        
        sqlString = "DELETE FROM FLICKR_HISTORY";
        cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &deleteStatement, nil)
    }
    
    @IBAction func clearAll(){
        
        sqlite3_bind_text(deleteStatement, 1, nil, -1, nil);
        if(sqlite3_step(deleteStatement) == SQLITE_DONE)
        {
            println("delete success")
        }else{
            //status.text = "Failed to delete contact";
            println("Error code: ", sqlite3_errcode(flickrDB));
            let error = String.fromCString(sqlite3_errmsg(flickrDB));
            println("Error message: ", error);
            println("delete fail")
        }
        sqlite3_reset(deleteStatement);
        sqlite3_clear_bindings(deleteStatement);
        
        self.objects.removeAllObjects();
        self.tableview.reloadData()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel!.text = objects[indexPath.row] as NSString
        return cell
    }
    

    
}