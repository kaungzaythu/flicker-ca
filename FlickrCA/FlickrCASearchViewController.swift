//
//  FlickrCASearchViewController.swift
//  FlickrCA
//
//  Created by Student on 7/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit

class FlickrCASearchViewController: UIViewController, UISearchBarDelegate, UITextFieldDelegate{
    var flickrHistory = FlickrCASearchHistory()
    var flickrRecent = FlickerCARecentViewController()
    var flickrDB : COpaquePointer = nil;
    var insertStatement : COpaquePointer = nil;
    
    
    @IBOutlet weak var txtNewSearch: UISearchBar!
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        txtNewSearch.resignFirstResponder()
        return true
    }

    override func viewDidLoad() {
        txtNewSearch.delegate = self;
        openDB()
        /*let yourImage = UIImage(named: "back.jpg")
        let imageview = UIImageView(image: yourImage)
        self.view.addSubview(imageview)*/
    }
    func openDB(){
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)[0] as String
        var docsDir = paths.stringByAppendingPathComponent("flickr.sqlite")
        if(sqlite3_open(docsDir, &flickrDB) == SQLITE_OK){
            var sql = "CREATE TABLE IF NOT EXISTS FLICKR_HISTORY (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT)"
            println(docsDir)
            var statement:COpaquePointer = nil
            if(sqlite3_exec(flickrDB, sql, nil, nil, nil) != SQLITE_OK){
                println("Failed to create table")
                println(sqlite3_errmsg(flickrDB));
            }
        }else{
            println("Failed to open table")
            println(sqlite3_errmsg(flickrDB));
        }
        prepareStatement();
    }
    
    
    func prepareStatement(){
        var sqlString: String
        sqlString = "INSERT INTO FLICKR_HISTORY (NAME) VALUES (?)"
        var cSql = sqlString.cStringUsingEncoding(NSUTF8StringEncoding)
        sqlite3_prepare_v2(flickrDB, cSql!, -1, &insertStatement, nil)
      
    }
    
    @IBAction func btnSearch() {
        txtNewSearch.resignFirstResponder()
        var namestr = (txtNewSearch.text as NSString).UTF8String
        sqlite3_bind_text(insertStatement, 1, namestr, -1, nil);
        if(sqlite3_step(insertStatement) == SQLITE_DONE)
        {
            println("success")
        }else{
            println("fail")
        }
        sqlite3_reset(insertStatement);
        sqlite3_clear_bindings(insertStatement);
        
        //flickrRecent.viewDidLoad();
        flickrRecent.loadHistoryTable(self);
        //flickrRecent.tableview.reloadData();
        
    }
    func textFieldShouldReturn(textField: UITextField!)->Bool{
        txtNewSearch.resignFirstResponder()
        return true;
    }
   
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        if segue.identifier == "SearchResult" {
            
            println(txtNewSearch.text)
            (segue.destinationViewController as FlickrCAResultViewController).searchkey = txtNewSearch.text
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
