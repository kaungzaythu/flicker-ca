//
//  AppDelegate.swift
//  FlickrCA
//
//  Created by Student on 7/2/15.
//  Copyright (c) 2015 Kaung Zay Thu. All rights reserved.
//

import UIKit
import AVFoundation
let themeColor = UIColor(red: (173/255.0), green: (216/255.0), blue: (230/255.0), alpha: 1.0)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    var window: UIWindow?
    var path = NSBundle.mainBundle().pathForResource("lethergo", ofType: "mp3")
    
    var soundTrack = AVAudioPlayer()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        /* Start to Play Music in background*/
        
        //println(path)
        
        soundTrack = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: path!), error: nil)
        
        soundTrack.numberOfLoops = -1
        
        soundTrack.volume = 0.35
        
        soundTrack.play()
        

        
        /* End to Play Music in background*/
        
        
        
       var tb:UITabBarController = window?.rootViewController as UITabBarController
        
       // var toViewController0:FlickrCASearchViewController = tb.viewControllers![0] as FlickrCASearchViewController;
        
        var navController = (tb.viewControllers![1] as UINavigationController)
        
       // var toViewController1:FlickrCAResultViewController = navController.childViewControllers[0] as FlickrCAResultViewController
        
        //toViewController1.searchResults = self.searchResults;
        
        //var toViewController2:GoogleResultWebViewController = tb.viewControllers![2] as GoogleResultWebViewController
        //toViewController2.searchResults = self.searchResults
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("switchWebView"), name: "SwitchWebView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("switchResultView"), name: "SwitchResultView", object: nil)
        
        return true

    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func switchResultView(){
        var tb:UITabBarController = window?.rootViewController as UITabBarController
        var fromView:UIView = tb.selectedViewController!.view;
        var toView:UIView! = tb.viewControllers?[1].view;
        
        UIView.transitionFromView(fromView, toView:toView, duration:0.5, options: UIViewAnimationOptions.TransitionCurlDown, completion:{(finished:Bool) -> Void in
            if(finished){
                tb.selectedIndex = 1;
            }
        });
    }
    func switchWebView(){
        var tb:UITabBarController = window?.rootViewController as UITabBarController
        var fromView:UIView = tb.selectedViewController!.view;
        var toView:UIView! = tb.viewControllers?[1].view;
        
        UIView.transitionFromView(fromView, toView:toView, duration:0.5, options: UIViewAnimationOptions.TransitionCurlDown, completion:{(finished:Bool) -> Void in
            if(finished){
                tb.selectedIndex = 2;
            }}
        );
    }
}

